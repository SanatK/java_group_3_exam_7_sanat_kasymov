package kg.attractor.fooddelivery.dto;
import kg.attractor.fooddelivery.model.Dish;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class DishDTO {

    public static DishDTO from(Dish dish) {
        return builder()
                .id(dish.getId())
                .restaurantId(dish.getRestaurantId())
                .name(dish.getName())
                .type(dish.getType())
                .price(dish.getPrice())
                .build();
    }
    private String id;
    private String restaurantId;
    private String name;
    private String type;
    private int price;
}
