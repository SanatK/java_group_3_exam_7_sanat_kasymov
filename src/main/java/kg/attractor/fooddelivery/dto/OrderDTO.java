package kg.attractor.fooddelivery.dto;

import kg.attractor.fooddelivery.model.Dish;
import kg.attractor.fooddelivery.model.Order;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class OrderDTO {

    public static OrderDTO from(Order order) {
        return builder()
                .id(order.getId())
                .userName(order.getUser().getName())
                .orderedDish(order.getOrderedDish())
                .restaurantName(order.getRestaurantName())
                .orderTime(order.getOrderTime())
                .build();
    }
    private String id;
    private String userName;
    private Dish orderedDish;
    private String restaurantName;
    private LocalDateTime orderTime;
}
