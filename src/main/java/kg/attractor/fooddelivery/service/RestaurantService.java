package kg.attractor.fooddelivery.service;

import kg.attractor.fooddelivery.dto.DishDTO;
import kg.attractor.fooddelivery.dto.RestaurantDTO;
import kg.attractor.fooddelivery.repository.DishRepository;
import kg.attractor.fooddelivery.repository.RestaurantRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;


@AllArgsConstructor
@Service
public class RestaurantService {
    private final RestaurantRepository resRepo;
    private final DishRepository dishRepo;

    public Slice<RestaurantDTO> getRestaurants(Pageable pageable) {
        var slice = resRepo.findAll(pageable);
        return slice.map(RestaurantDTO::from);
    }
    public Slice<DishDTO> getDishes(String restaurantId, Pageable pageable){
        var slice = dishRepo.findAllByRestaurantId(restaurantId, pageable);
        return slice.map(DishDTO::from);
    }
}
