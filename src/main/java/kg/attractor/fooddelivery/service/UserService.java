package kg.attractor.fooddelivery.service;

import kg.attractor.fooddelivery.dto.DishDTO;
import kg.attractor.fooddelivery.dto.OrderDTO;
import kg.attractor.fooddelivery.dto.RestaurantDTO;
import kg.attractor.fooddelivery.dto.UserDTO;
import kg.attractor.fooddelivery.model.Order;
import kg.attractor.fooddelivery.model.User;
import kg.attractor.fooddelivery.repository.DishRepository;
import kg.attractor.fooddelivery.repository.OrderRepository;
import kg.attractor.fooddelivery.repository.RestaurantRepository;
import kg.attractor.fooddelivery.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepo;
    private final OrderRepository orderRepo;
    private final DishRepository dishRepo;
    private final RestaurantRepository resRepo;
    private final PasswordEncoder encoder;

    public List<OrderDTO> getUserOrders(String userId){
        List<Order> orders = orderRepo.findAllByUser_Id(userId);
        List<OrderDTO> ordersDTO = new ArrayList<>();
        for(Order o: orders){
            OrderDTO.from(o);
            ordersDTO.add(OrderDTO.from(o));
        }
        return ordersDTO;
    }
    public OrderDTO createOrder(String dishId, String userId){
        var order = Order.builder()
                .user(userRepo.findById(userId).get())
                .orderedDish(dishRepo.findById(dishId).get())
                .restaurantName(resRepo.findById(dishRepo.findById(dishId).get().getRestaurantId()).get().getName())
                .orderTime(LocalDateTime.now())
                .build();
        orderRepo.save(order);
        return OrderDTO.from(order);
    }
    public UserDTO addUser(UserDTO userData){
        var user = User.builder()
                .id(userData.getId())
                .email(userData.getEmail())
                .name(userData.getName())
                .password(encoder.encode(userData.getPassword()))
                .build();
        userRepo.save(user);
        return UserDTO.from(user);
    }
}
