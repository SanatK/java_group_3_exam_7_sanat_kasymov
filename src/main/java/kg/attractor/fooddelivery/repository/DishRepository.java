package kg.attractor.fooddelivery.repository;

import kg.attractor.fooddelivery.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface DishRepository extends PagingAndSortingRepository<Dish, String> {
    Page <Dish>findAllByRestaurantId(String restaurantId, Pageable pageable);
}
