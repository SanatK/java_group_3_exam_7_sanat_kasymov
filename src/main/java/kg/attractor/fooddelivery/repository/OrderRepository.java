package kg.attractor.fooddelivery.repository;

import kg.attractor.fooddelivery.model.Order;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, String> {
    List<Order> findAllByUser_Id(String customerId);
}
