package kg.attractor.fooddelivery.repository;

import kg.attractor.fooddelivery.model.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RestaurantRepository extends PagingAndSortingRepository<Restaurant, String> {
    @Override
    Page<Restaurant> findAll(Pageable pageable);
}
