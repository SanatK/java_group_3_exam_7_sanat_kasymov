package kg.attractor.fooddelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@AllArgsConstructor
@Builder
@Document
@Data
public class Dish {
    @Id
    private String id;
    private String restaurantId;
    private String name;
    private String type;
    private int price;

}
