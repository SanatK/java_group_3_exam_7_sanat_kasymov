package kg.attractor.fooddelivery.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.UUID;
import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Document
@Data
public class Order {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private User user;
    private String restaurantName;
    private Dish orderedDish;
    private LocalDateTime orderTime;
}
