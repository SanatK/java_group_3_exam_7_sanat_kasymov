package kg.attractor.fooddelivery.controller;
import kg.attractor.fooddelivery.dto.OrderDTO;
import kg.attractor.fooddelivery.dto.UserDTO;
import kg.attractor.fooddelivery.model.User;
import kg.attractor.fooddelivery.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@AllArgsConstructor
@RestController
@RequestMapping("*")
public class UserController {
    private final UserService userService;


    /*
    форма для регистрации
    {
        "id":"u0004",
        "email":"bema@gmail.com",
        "name":"bema",
        "password":"1010"
    }
    */


    @PostMapping("/register")
    public UserDTO registerUser(@RequestBody UserDTO userData){
        return userService.addUser(userData);
    }


    /* путь /user/** для авторизованных пользователей*/
    @GetMapping("/user")
    public List<OrderDTO> getUserOrders(Authentication authentication){
        User user = (User) authentication.getPrincipal();
        return userService.getUserOrders(user.getId());
    }
    @PostMapping("/user")
    public OrderDTO createOrder(Authentication authentication, @RequestParam("dishId") String dishId){
        User user = (User) authentication.getPrincipal();
        return userService.createOrder(dishId, user.getId());
    }
}
