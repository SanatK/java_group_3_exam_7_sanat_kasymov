package kg.attractor.fooddelivery.controller;

import kg.attractor.fooddelivery.annotations.ApiPageable;
import kg.attractor.fooddelivery.dto.DishDTO;
import kg.attractor.fooddelivery.dto.RestaurantDTO;
import kg.attractor.fooddelivery.service.RestaurantService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
@AllArgsConstructor
@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    private final RestaurantService restaurantService;


    /*
    путь Swagger - http://localhost:8080/swagger-ui.html#/
    */



    @ApiPageable
    @GetMapping
    public Slice<RestaurantDTO> getRestaurants(@ApiIgnore Pageable pageable){
        return restaurantService.getRestaurants(pageable);
    }
    @ApiPageable
    @GetMapping("/{restaurantId}")
    public Slice<DishDTO> getDishes(@PathVariable String restaurantId, @ApiIgnore Pageable pageable){
        return restaurantService.getDishes(restaurantId, pageable);
    }

}
