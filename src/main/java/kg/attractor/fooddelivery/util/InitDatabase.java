package kg.attractor.fooddelivery.util;

import kg.attractor.fooddelivery.model.Dish;
import kg.attractor.fooddelivery.model.Restaurant;
import kg.attractor.fooddelivery.repository.DishRepository;
import kg.attractor.fooddelivery.repository.RestaurantRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class InitDatabase {


    @Bean
    CommandLineRunner init(DishRepository dishRepo, RestaurantRepository restaurantRepo) {
        return (args) -> {
            dishRepo.deleteAll();
            restaurantRepo.deleteAll();
            restaurantRepo.saveAll(getRestaurants());
            dishRepo.saveAll(getDishes());
        };
    }
    public List<Restaurant> getRestaurants(){
        List<Restaurant> restaurants = new ArrayList<>();
        for(int i = 0; i<10; i++){
            restaurants.add(new Restaurant(String.format("%s%s", "res", i), GenerateData.randomPlace().getName(), GenerateData.randomPlace().getDescription()));
        }
        return restaurants;
    }
    public List<Dish> getDishes(){
        List<Dish> dishes = new ArrayList<>();
        Random ran = new Random();
        int random;
        for(int i = 0; i<35; i++){
            random = ran.nextInt(getRestaurants().size());
            String resId = getRestaurants().get(random).getId();
            dishes.add(new Dish(String.format("%s%s", "dish", i), resId, GenerateData.randomDish().getName(), GenerateData.randomDish().getType(), ran.nextInt(100)+1));
        }
        return dishes;
    }
}